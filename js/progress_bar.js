!function ($) {
  $(function () {
    var progress_bar_info = {
      enable: true,
      percent: 0,
      width: 100,
      height_ratio: 10,
      bgcolor: '#eee',
      animate: true,
      easing: 'linear',
      max_percent: 100,
      min_percent: 0
    };

    $.fn.set_progress_bar = function (v) {
      $.extend(progress_bar_info, v);
      if (progress_bar_info.percent > progress_bar_info.max_percent) {
        progress_bar_info.percent = progress_bar_info.max_percent;
      } else if (progress_bar_info.percent < progress_bar_info.min_percent) {
        progress_bar_info.percent = progress_bar_info.min_percent;
      }
    };

    $.fn.progress_bar = function () {
      if (progress_bar_info.enable == false) return false;
      var progress_width = progress_bar_info.width * (progress_bar_info.percent / 100);
      $('.progress_bar').css({
        "width" : progress_bar_info.width + "px",
        "height" : (progress_bar_info.width / progress_bar_info.height_ratio) + "px",
      });
      $('.progress_bar').append('<div class="progress_bar_content"></div>');
      if (progress_bar_info.percent == progress_bar_info.max_percent) {
        $('.progress_bar_content').css({
          "-webkit-border-radius" : "5px",
          "-moz-border-radius" : "5px",
          "-o-border-radius" : "5px",
          "border-radius" : "5px"
        });
      }
      var progress_bar_css = {
        "width" : progress_width + "px",
        "background-color" : progress_bar_info.bgcolor
      };
      if (progress_bar_info.animate) {
        $('.progress_bar_content').animate(progress_bar_css, 1000, progress_bar_info.easing);
      } else {
        $('.progress_bar_content').css(progress_bar_css);
      }
    };
  });
}(window.jQuery);
